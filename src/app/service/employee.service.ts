import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  userApiUrl = "http://localhost:3000/users";
  constructor(private http:HttpClient) { }

  getEmployee(){
    return this.http.get(this.userApiUrl);
  }
  SaveEmployee(inputdata:any){
    return this.http.post(this.userApiUrl, inputdata);
  }

  removeEmployee(id:any){
    return this.http.delete(this.userApiUrl+'/' +id)
  }

  loadDesignation(){
    return this.http.get(this.userApiUrl);
  }
}
