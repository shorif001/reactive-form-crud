import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EmployeeService } from '../service/employee.service';

@Component({
  selector: 'app-employee-add',
  templateUrl: './employee-add.component.html',
  styleUrls: ['./employee-add.component.scss']
})
export class EmployeeAddComponent implements OnInit {

  constructor(private empService:EmployeeService) { }

  ngOnInit(): void {
  }

  errorMessage = '';
  errorClass = '';
  
  empform = new FormGroup({
    id:new FormControl(''),
    name: new FormControl('', [Validators.required, Validators.minLength(5)]),
    email: new FormControl('', [Validators.required, Validators.email]),
    phone:new FormControl(),
    designation:new FormControl(),
  })


  get name(){
    return this.empform.get("name");
  }
  get email(){
    return this.empform.get("email");
  }


  saveresponse:any
  addUser(){
    // console.log(this.empform.value);
    if(this.empform.valid){
      this.empService.SaveEmployee(this.empform.getRawValue()).subscribe(result=>{
        this.saveresponse=result;
        // console.log(this.saveresponse);
        if(this.saveresponse.result=='pass'){
          this.errorMessage = "Failed to save"
          this.errorClass = 'validerror'
        }
      })
      
    }else{
      this.errorMessage='Please Enter valid data';
      this.errorClass = "validerror";
    }
    
  }
  
}
