import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../service/employee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {

  constructor(private userService:EmployeeService) { 
    this.getEmployee();
  }

  ngOnInit(): void {
  }

  emplist:any;
  
  getEmployee(){
    this.userService.getEmployee().subscribe(result=>{
      this.emplist = result;
    })
  }

  functionedit(edit:any){

  }


  onDeleteUser(id:number){
    if(confirm('Do you want to delete this user?')){
      // this.posts.splice(id,1);
      // this.emplist.splice(id,1);
      this.userService.removeEmployee(id).subscribe(result=>{
        this.emplist = result;
      })
    }
  }



  
}
